/*
 * This file is part of eyeo's Web Extension Ad Blocking Toolkit (EWE),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * EWE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * EWE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EWE.  If not, see <http://www.gnu.org/licenses/>.
 */

import browser from "webextension-polyfill";

import {filterEngine} from "adblockpluscore/lib/filterEngine.js";
import {synchronizer} from "adblockpluscore/lib/synchronizer.js";
import {notifications as _notifications}
  from "adblockpluscore/lib/notifications.js";

import * as frameState from "./frame-state.js";
import * as requestFilter from "./request-filter.js";
import * as messageResponder from "./message-responder.js";
import * as popupBlocker from "./popup-blocker.js";
import {init as initIDB} from "./idb.js";
import {configureDefaultSubscriptions} from "./default-subscriptions.js";
import {setSnippetLibrary} from "./content-filter.js";

export {onSubscribeLinkClicked} from "./subscribe-links.js";
export {default as subscriptions} from "./subscriptions.js";
export {default as filters} from "./filters.js";
export {default as reporting} from "./reporting.js";
export {default as debugging} from "./debugging.js";

/**
 * The notifications API (only used by Adblock Plus).
 * @see {@link https://eyeo.gitlab.io/adblockplus/adblockpluscore/master/docs/module-notifications.html|Adblock Plus core notifications documentation}
 * @external
 */
// Workaround for documentation.js to parse the JSDocs above
// https://github.com/documentationjs/documentation/issues/1363
export let notifications = _notifications;

export let snippets = {
  /**
   * Enables support for snippet filters.
   * @param {string} code The code defining the available snippets.
   */
  setLibrary: setSnippetLibrary
};

let stopped = true;

/**
 * Initializes the filter engine and starts blocking content.
 * @return {Promise.<undefined>}
 */
export async function start()
{
  stopped = false;

  await initIDB();
  if (stopped)
    return;

  // https://bugs.chromium.org/p/chromium/issues/detail?id=1159438
  notifications.locale = browser.i18n.getUILanguage ?
    browser.i18n.getUILanguage() : navigator.language;
  notifications.start();

  await filterEngine.initialize();
  if (stopped)
    return;

  synchronizer.start();
  configureDefaultSubscriptions();

  await frameState.start();
  if (stopped)
    return;

  requestFilter.start();
  popupBlocker.start();
  messageResponder.start();
}

/**
 * Stops blocking any content.
 */
export function stop()
{
  stopped = true;
  synchronizer.stop();
  frameState.stop();
  requestFilter.stop();
  popupBlocker.stop();
  messageResponder.stop();
  notifications.stop();
}
